#include<stdio.h>

// merge function is the same as above
void iterativeMergeSort(int arr[], int n)
{
   int size; 
   int start; 
   int mid;
   int end;
   for ( size=1; size<n ; size = 2* size)
   {
       for ( start=0;  start<n-1;  start += 2*size)
       {
           mid = start + size - 1;
           end = min( start + 2*size - 1, n-1);
           merge(arr, start, mid, end);
       }
   }
}

int min(int a, int b)
{
    if(a < b)
	return a;
    else
	return b;
}

void merge(int arr[], int l, int m, int r)
{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;
    int L[n1], R[n2];
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)       
 R[j] = arr[m + 1+ j];
 
    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2)
  {
   if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }
 
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }
 
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

int main()
{
    int arr_size = 10;
    int arr[arr_size];
    //int arr[10] = {1,12,3,34,4,45,65,2,22,15};
    iterativeMergeSort(arr,arr_size);
    //int i = 0;
    //for(i=0;i<10;i++)
    //	printf("%d ",arr[i]);
    return 0;
}

