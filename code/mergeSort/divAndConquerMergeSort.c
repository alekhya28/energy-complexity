#include<stdio.h>

void iterativeMergeSort(int arr[], int n)
{
   int size; 
   int start; 
   int mid;
   int end;
   for ( size=1; size<n ; size = 2* size)
   {
       for ( start=0;  start<n-1;  start += 2*size)
       {
           mid = start + size - 1;
           end = min( start + 2*size - 1, n-1);
           merge(arr, start, mid, end);
       }
   }
}

int min(int a, int b)
{
    if(a < b)
	return a;
    else
	return b;
}

void divAndConqMergeSort(int arr[], int size, int parts )
{
    int i;
// assuming for now that the array can be divided evenly into those many parts
	for( i=0; i<size; i+= size/parts){
		iterativeMergeSort( &arr[i] , size/parts);
	}

    for(i=0;i<size;i++)
   	printf("%d ",arr[i]);
    printf("\n");

// After we have sorted the individual parts we then merge them again
   int cur_size; 
   int start; 
   int mid;
   int end;
   for ( cur_size= size/parts; cur_size<size ; cur_size = 2* cur_size)
   {
       for ( start=0;  start<size-1;  start += 2*cur_size)
       {
           mid = start + cur_size - 1;
           end = min( start + 2*cur_size - 1, size-1);
           merge(arr, start, mid, end);
       }
   }
}


void merge(int arr[], int l, int m, int r)
{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;
    int L[n1], R[n2];
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)       
 R[j] = arr[m + 1+ j];
 
    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2)
  {
   if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }
 
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }
 
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

int main()
{
    int arr_size = 8;
    int parts = 2;
    int arr[arr_size];
    //int arr[8] = {1,12,3,34,4,45,2,22};
    divAndConqMergeSort(arr,arr_size,parts);
    //int i = 0;
    //for(i=0;i<arr_size;i++)
    //	printf("%d ",arr[i]);
    return 0;
}

