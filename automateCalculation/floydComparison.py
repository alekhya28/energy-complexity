from math import log
import matplotlib.pyplot as plt
X = []
n = 50
normal = n*n*n*(1 + 2*n*n)


def doubleInput(start, stop):
	while start <= stop:
		yield start
		start <<= 1

def DivEnergy1(p):
	ans = 0
	a = p*p*p
	b = p*p
	for k in range(1,n/p+1):
		temp1 = k*k*k - (k-1)*(k-1)*(k-1)
		temp1 *= a
		temp2 = temp1
		temp2 *= 2*k*k*p*p
		ans += temp1 + temp2
	return ans;

def DivEnergy2(p):
	ans = 0
	# a = n*p*p + (n*(n+p)*(2*n+p)*p*p)/3
	a = n*p*p + p*p*p*n*(n+p)
	b = n*n*n - n*p*p
	c = b*2*n*n
	ans = a+b+c
	return ans

# def DivEnergy3(p):
# 	ans = 0
# 	a = p*p*p
# 	b = p*p
# 	for k in range(1,n/p+1):
# 		# print k
# 		temp1 = k*k*k - (k-1)*(k-1)*(k-1) -1
# 		temp1 *= a
# 		temp2 = temp1
# 		temp2 *= 2*k*k*p*p
# 		ans += temp1 + temp2 +  n*p*p + (n*(n+p)*(2*n+p)*p*p)/3
# 	return ans;


Y = []
Z = []
p_arr = []
actual = []
for p in range(1, n+1):

	if n%p !=0 :
		continue
	p_arr.append(p)
	calc = DivEnergy1(p)
	X.append(calc)
	# calc = DivEnergy2(p)
	# Y.append(calc)
	# calc = DivEnergy3(p)
	actual.append(normal)

print X
print Y
print normal

plt.figure()
plt.xlabel("Value of p")
plt.ylabel("Total Energy")
plt.title("Floyd Warshall Energy Comparison")
a, = plt.plot(p_arr, X, marker = 'o')
# b, = plt.plot(p_arr, Y, marker = 'o')
# c, = plt.plot(p_arr, Z, marker ='o')	
c, = plt.plot(p_arr, actual, marker ='o')
plt.legend([a, c],[ "Divide and Conquer 1", "Iterative"])
plt.savefig("final_plots/floyd.png")
plt.show()

