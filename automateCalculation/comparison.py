from math import log
import numpy as np  
import matplotlib.pyplot as plt

X = []
n = 128
normal = n*n*log(n,2)
# formula =n*p*log(p,2) + (n*n*log(n/p))/2 + n*n - n*p
# formula = 'p'

def graph():  
	for a in doubleInput(1,n):
		print a
		x=[]
		z = []
		y = []
		for p in range(1,a+1):
			x.append(p)
			y.append(a*p*log(p,2) + (a*a*log(a/p))/2 + a*a - a*p)
			z.append(abs(a - 2*p*(log(p,2)+1)))
		plt.plot(x, y)  
		print y.index(min(y))+1
		print z.index(min(z))+1
	plt.show()


def doubleInput(start, stop):
	while start <= stop:
		yield start
		start <<= 1

def inplaceMerge(p):
	ans = 0
	a = n*n*n
	b = n*n*p
	c = (2*n*n*n)/3
	d = (2*n*p*p)/3
	e = n*p*p
	ans = a-b+c-d+e
	return ans

def DivEnergy(p):

	eqn = 0
	t1 = n*n*log(p,2) + n*p*log(p,2)
	t1 = t1/2
	t2 = n*n*log(n/p,2)
	t2 /= 2
	t3 = n*n
	t4 = n*p

	eqn = t1+t2+t3-t4
	return eqn
	# ans = 0
	# a = n/p
	# b = a+1
	# c = 2*a+1

	# temp1 = a*a*b*b
	# temp1 /=16

	# temp2 = a*b*c
	# temp2 /= 12

	# print temp1, temp2
	# # temp1 = n/p*(n/p+1)*((3*n)/p + 1)(n/p+2);
	# # temp1 /= 24
	# # temp2 = n/p*(n/p+1)
	# # temp2 /= 2
	# ans = temp1 + temp2 - 3/4
	# ans *= p*p

	# ans += n*p*log(p,2)

	ans = 0
	# print "P = "+ str(p)
	for i in doubleInput(2, n/p):
		for j in range(i,n/p+1,i):
			# print i,j
			ans += i*j

	ans *= p*p

	ans += (n*p+n*n)*log(p,2)/2

	return ans;

Y = []
p_arr = []
actual = []
a1 = 2*n*n
a2 = 1 - 1/2*n
actual_ans = a1*a2
actual_M = []
rec = []
rec_en = n*n*log(n,2)+(n*log(n,2)*(log(n,2)+1))/2

for p in doubleInput(1, n):
	calc = DivEnergy(p)
	X.append(calc)
	# calc = inplaceMerge(p)
	# Y.append(calc)
	p_arr.append(log(p,2))
	# actual.append(a1*a2*n)
	rec.append(rec_en)
	actual_M.append(normal)


print X
# print Y
print normal
# print n*n*n
plt.figure()
plt.xlabel("Value of p")
plt.ylabel("Additional Alive Energy")
plt.title("Energy Comparison of Merge Sort using extra space")
a, = plt.plot(p_arr, actual_M, marker ='o')
b, = plt.plot(p_arr, X, marker ='o')
c, = plt.plot(p_arr, rec, marker = 'o')
plt.legend([c, a, b],[ "Recursive Merge Sort", "Iterative Merge Sort", "Divide and conquer Merge Sort"])
plt.savefig("final_plots/div_merge_extra.png")
plt.show()

# print pow(2, X.index(min(X)))
# graph()

	

