from math import log
import matplotlib.pyplot as plt
from matplotlib.pyplot import bar

energy_arr = []
energy_IM_arr = []

def doubleInput(start, stop):
	while start <= stop:
		yield start
		start <<= 1

def P(n):
	p = 19
	p += 63*n
	return p

def TimeComplexity(n):
	t = 0
	for i in doubleInput(1,n-1):
		t += i*(29 + P(n/i))
	t += 6*n
	return t

def IM(n):
	p = 10
	p += 19*n
	p += (15*n*n)/4
	return p

def TimeComplexityIM(n):
	t = 0
	for i in doubleInput(1,n-1):
		t += i*(32 + IM(n/i));
	t += 6*n
	# print t
	return t

def E(n):
	return (19+63*n)*(n+9);

def SpaceComplexity(n):
	t = TimeComplexity(n)
	main = (t+6)*n
	mergesort = 0
	for i in doubleInput(1,n-1):
		mergesort += i*TimeComplexity(n/i)

	mergesort += 6*n
	mergesort *= 4

	merge = 0
	for i in doubleInput(1,n-1):
		merge += i*E(n/i)

	return main+merge+mergesort

def EIM(n):
	return 7*IM(n)

def SpaceComplexityIM(n):
	t = TimeComplexityIM(n)
	main = (t + 6)*n;
	mergesort = 0;
	i = 0;

	for i in doubleInput(1,n-1):
		mergesort += i*TimeComplexityIM(n/i);
	mergesort += 6*n;
	mergesort *= 4;

	merge = 0;
	for i in doubleInput(1,n-1):
		merge += i*EIM(n/i);

	return main+merge+mergesort; 

def calculateEnergy(n):
	timecomp = TimeComplexity(n) + 6;
	space = SpaceComplexity(n);
	return timecomp+space;

def calculateEnergyIM(n):
	timecomp = TimeComplexityIM(n) + 6;
	space = SpaceComplexityIM(n);
	return timecomp+space;

x = []
time_arr = []
alive_arr = []
y= []

for i in doubleInput(2,16):
	energy = calculateEnergy(i)
	# print i
	t = TimeComplexity(i)
	a = SpaceComplexity(i)
	time_arr.append(t)
	alive_arr.append(a)
	# print energy
	energy_arr.append(energy)

	energyIM = calculateEnergyIM(i)
	# print energyIM
	energy_IM_arr.append(energyIM)
	# x.append(log(i,2))
	# y.append(log(i,2)+0.2)
	x.append(i)



# plt.figure()
# plt.xlabel('Array size')
# plt.ylabel('Energy')
# plt.title('Energy Analysis recursive merge sort using extra space')
# a, = plt.plot(x, energy_arr, marker= 'o', color='green')
# b, = plt.plot(x, time_arr, marker ='o', color='blue')
# c, = plt.plot(x, alive_arr, marker ='o', color='red')
# plt.legend([a, b, c],["Total Energy", "Total Number of steps", "Total Alive energy"])
# plt.savefig("plots/rec_extra.png")
# plt.show()

# plt.figure();
# plt.ylabel('Energy')
# plt.tick_params(labelbottom='off') 
# # plt.axes().get_xaxis().set_visible(False)
# plt.xlabel("8                         16                         32                            64")
# plt.title('Energy analysis of variants of recursive mergesort')
# bar(x, energy_arr,width=0.2,color='gold',align='center', label = "Using auxiliary array")
# bar(y, energy_IM_arr,width=0.2,color='green',align='center', label = "Inplace merge")
# plt.legend()
# plt.savefig("plots/rec_comparison.png")
# plt.show()
