#include <stdio.h>

int P(int n)
{
	int p = 19;
	p += 63.5*n;
	return p;
}

int TimeComplexity( int n)
{
	int t = 0;
	int i = 1;
	for(i=1; i<n ; i*=2)
	{
		t += i*(29 + P(n/i));
	}

	t += 3*n;
	// printf("%d\n",t);
	return t;
} 

int IM(int n)
{
	int p = 22;
	p += 12*n;
	p += (15*n*n)/4;
	return p;
}

int TimeComplexityIM( int n)
{
	int t = 0;
	int i = 1;
	for(i=1; i<n ; i*=2)
	{
		t += i*(32 + IM(n/i));
	}

	t += 3*n;
	// printf("%d\n",t);
	return t;
} 

int E(int n)
{
	return (19+63.5*n)*(n+9);
}

int SpaceComplexity( int n )
{
	int t = TimeComplexity(n);
	int main = (t + 7)*n;
	int mergesort = 0;
	int i = 0;
	for(i=1;i<n;i*=2)
		mergesort += i*TimeComplexity(n/i);
	mergesort += 3*n;
	mergesort *= 4;

	int merge = 0;
	for(i=1;i<n;i*=2)
		merge += i*E(n/i);


	int sum = main+merge+mergesort;
	return sum;
}

int EIM(int n)
{
	return 7*IM(n);
}

int SpaceComplexityIM( int n )
{
	int t = TimeComplexityIM(n);
	int main = (t + 7)*n;
	int mergesort = 0;
	int i = 0;
	for(i=1;i<n;i*=2)
		mergesort += i*TimeComplexityIM(n/i);
	mergesort += 3*n;
	mergesort *= 4;

	int merge = 0;
	for(i=1;i<n;i*=2)
		merge += i*EIM(n/i);

	// printf("%d\n",main+mergesort+merge);
	return main+merge+mergesort;

}

int calculateEnergy(int n)
{
	int timecomp = TimeComplexity(n) + 7;
	int space = SpaceComplexity(n);
	return timecomp+space;
}

int calculateEnergyIM(int n)
{
	int timecomp = TimeComplexityIM(n) + 7;
	int space = SpaceComplexityIM(n);
	return timecomp+space;
}

int main()
{
	printf("RECURSIVE MERGESORT\n");
	for(int i=2;i<=128 ; i*=2)
	{
		printf("%d :- \n", i );
		int energy = calculateEnergy(i);
		printf("copy merge %d\n", energy);
		int energyIM = calculateEnergyIM(i);
		printf("inplace merge %d\n", energyIM);
	}
	
}

