from math import log
import numpy as np  
import matplotlib.pyplot as plt

X = []
n = 128


def doubleInput(start, stop):
	while start <= stop:
		yield start
		start <<= 1

def inplaceMerge(p):
	ans = 0
	a = n*n*n
	b = n*n*p
	c = (2*n*n*n)/3
	d = (2*n*p*p)/3

	e = (n*(2*p-1)*(n+p))/2
	ans = a-b+c-d+e
	return ans

Y = []
p_arr = []
a1 = n*n*(2*n-1)
actual_ans = a1
actual_M = []
rec = []
rec_en = a1+n*n*log(n,2)+(n*log(n,2)*(log(n,2)+1))/2

for p in doubleInput(1, n):
	calc = inplaceMerge(p)
	Y.append(calc)
	p_arr.append(log(p,2))
	# actual.append(a1*a2*n)
	rec.append(rec_en)
	actual_M.append(actual_ans)

print Y
print actual_ans

plt.figure()
plt.xlabel("Value of p")
plt.ylabel("Additional Alive Energy")
plt.title("Energy Comparison of In Place Merge Sort")
a, = plt.plot(p_arr, actual_M, marker ='o')
b, = plt.plot(p_arr, Y, marker ='o')
c, = plt.plot(p_arr, rec, marker = 'o')
plt.legend([a, b, c],[ "Iterative Merge Sort", "Divide and conquer Merge Sort", "Recursive Merge Sort"])
plt.savefig("final_plots/div_inplace_merge.png")
plt.show()

# print pow(2, X.index(min(X)))
# graph()

	

