from math import log
import matplotlib.pyplot as plt
from matplotlib.pyplot import bar
import iterative_mergesort
import rec_mergesort

energy_arr = []
energy_IM_arr = []

def doubleInput(start, stop):
	while start <= stop:
		yield start
		start <<= 1

def div_P(n):
	p = 19
	p += 63*n
	return p


def div_IM(n):
	p = 10
	p += 19*n
	p += (15*n*n)/4
	return p

def div_TimeComplexity(n,p):
	t = 8
	t += 19*p
	t += p*iterative_mergesort.TimeComplexity(n/p)
	t += log(n,2)*7
	t += (p-1)*36

	# print p

	# print n/(2*p)
	for i in doubleInput(1,p/2):
		t += i*(div_P(n/i))
	# 	print i

	return t

def div_TimeComplexity_IM(n,p):
	t = 8
	t += 19*p
	t += p*iterative_mergesort.TimeComplexityIM(n/p)
	t += log(n,2)*7
	t += (p-1)*34

	# print p

	# print n/(2*p)
	for i in doubleInput(1,p/2):
		t += i*(div_IM(n/i))
	# 	print i

	return t


def div_E(n):
	return (19+63*n)*(n+9);


def div_EIM(n):
	return 7*div_IM(n);

def div_SpaceComplexity(n,p):
	t = div_TimeComplexity(n,p)
	main = (t+5)*n
	mergesort = 0
	# mergesort = p*div_M(n/p)*6

	mini = 8*(p-1) + p*(n/p-1)*8

	merge = 0
	for i in doubleInput(1,p/2):
		merge += i*div_E(n/i)

	for i in doubleInput(1,(n/2*p)):
		merge += p*(i*div_E(n/(i*p)))

	it_ms = p*iterative_mergesort.TimeComplexity(n/p)*6;
	div_ms = t*8
	return main+merge+it_ms+mini+div_ms


def div_SpaceComplexity_IM(n,p):
	t = div_TimeComplexity_IM(n,p)
	main = (t+5)*n
	mergesort = 0
	# mergesort = p*div_M_IM(n/p)*6

	mini = 8*(p-1) + p*(n/p-1)*8

	merge = 0
	for i in doubleInput(1,p/2):
		merge += i*div_EIM(n/i)

	for i in doubleInput(1,(n/2*p)):
		merge += p*(i*div_EIM(n/(i*p)))

	it_ms = p*iterative_mergesort.TimeComplexityIM(n/p)*4;
	div_ms = t*7

	# print main+merge+mergesort+mini+it_ms+div_ms
	return main+merge+it_ms+mini+div_ms

# def EIM(n):
# 	return 7*IM(n)

# def SpaceComplexityIM(n):
# 	t = TimeComplexityIM(n)
# 	main = (t + 7)*n;
# 	mergesort = 0;
# 	i = 0;

# 	for i in doubleInput(1,n-1):
# 		mergesort += i*TimeComplexityIM(n/i);
# 	mergesort += 3*n;
# 	mergesort *= 4;

# 	merge = 0;
# 	for i in doubleInput(1,n-1):
# 		merge += i*EIM(n/i);

# 	return main+merge+mergesort; 

def div_calculateEnergy(n,p):
	timecomp = div_TimeComplexity(n,p) + 5;
	space = div_SpaceComplexity(n,p);
	return timecomp+space;

def div_calculateEnergy_IM(n,p):
	timecomp = div_TimeComplexity_IM(n,p) + 5;
	space = div_SpaceComplexity_IM(n,p);
	return timecomp+space;

# def calculateEnergyIM(n):
# 	timecomp = TimeComplexityIM(n) + 7;
# 	space = SpaceComplexityIM(n);
# 	return timecomp+space;

x = []
p = []
q = []
y = []
z = []
k = []
a = []
b = []
c = []
d = []

r1 = []
s1 = []

r2 = []
s2 = []


energy_arr =[]
time_arr = []
alive_arr = []
energy_IM_arr = []
x1 = []
y1 = []

for i in doubleInput(64, 256):


	energy = div_calculateEnergy_IM(i, 1)
	y.append(energy)
	print energy

	energy = div_calculateEnergy_IM(i, 4)
	c.append(energy)

	energy = div_calculateEnergy_IM(i, 8)
	z.append(energy)

	# energy = div_calculateEnergy_IM(i, 8)
	# energy_IM_arr.append(energy)


	# time_arr.append(div_TimeComplexity(i,8))
	# alive_arr.append(div_SpaceComplexity(i,8))

	# x1.append(log(i,2))
	# y1.append(log(i,2)+0.2)
	# z.append(energy)

	energy = div_calculateEnergy_IM(i, 16)
	d.append(energy)

	energy = div_calculateEnergy_IM(i, 32)
	k.append(energy)
	# print energy

	energy1 = iterative_mergesort.calculateEnergyIM(i)
	r1.append(energy1)
	print energy1
	print "\n"

	energy2 = rec_mergesort.calculateEnergyIM(i)
	s1.append(energy2)


	for j in doubleInput(2,i):
		e = div_calculateEnergy_IM(i, j)
		if(e < energy1):
			print "YES"


	
	# print energy
	# print "\n"
	
	p.append(int(10*log(i,2)-3))
	a.append(int(10*log(i,2)-2))
	x.append(int(10*log(i,2)-1))
	b.append(int(10*log(i,2)))
	q.append(int(10*log(i,2)+1))
	r2.append(int(10*log(i,2)+2))
	s2.append(int(10*log(i,2)+3))


# plt.figure();
# plt.ylabel('Energy')
# plt.tick_params(labelbottom='off') 
# # plt.axes().get_xaxis().set_visible(False)
# plt.xlabel("8                         16                         32                            64")
# plt.title('Energy analysis of variants of divide and conquer mergesort')
# bar(x1, energy_arr,width=0.2,color='orange',align='center', label = "Using auxiliary array")
# bar(y1, energy_IM_arr,width=0.2,color='purple',align='center', label = "Inplace merge")
# plt.legend()
# plt.savefig("plots/div_comparison.png")
# plt.show()

# plt.figure()
# plt.xlabel('Array size')
# plt.ylabel('Energy')
# plt.title('Energy Analysis Divide and conquer mergesort using extra space')
# a, = plt.plot(x, energy_arr, marker= 'o', color='green')
# b, = plt.plot(x, time_arr, marker ='o', color='blue')
# c, = plt.plot(x, alive_arr, marker ='o', color='red')
# plt.legend([a, b, c],["Total Energy", "Total Number of steps", "Total Alive energy"])
# plt.savefig("plots/div_extra.png")
# plt.show()

plt.figure();
plt.ylabel('Energy')
plt.tick_params(labelbottom='off') 
# plt.axes().get_xaxis().set_visible(False)
plt.xlabel("64                                   128                                    256")
plt.title('Comparing energy complexities of in place merge sorts')
bar(p, y,width=1,color='blue',align='center', label = "p=2")
bar(a, c,width=1,color='gold',align='center', label = "p=4")
bar(x, z,width=1,color='red',align='center', label = "p=8")
bar(b, d,width=1,color='purple',align='center', label = "p=16")
bar(q, k,width=1,color='green',align='center', label = "p=32")
bar(r2, r1,width=1,color='orange',align='center', label = "iterative", hatch = "o")
bar(s2, s1,width=1,color='indigo',align='center', label = "recursive", hatch = ".")
plt.legend()
# plt.show()
plt.savefig("plots/5.png")
plt.show()

# plt.figure()
# plt.xlabel('Array size')
# plt.ylabel('Energy')
# plt.title('Energy complexity of recursive merge sort')
# a, = plt.plot(x,energy_arr, marker= 'o')
# b, = plt.plot(x, energy_IM_arr, marker ='o')
# plt.legend([a, b],["Extra space merge", "In place merge"])
# plt.savefig("plots/div_E.png")
# plt.show()